<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
<title><?php print $head_title ?></title>
	<?php print $head ?>
	<?php print $styles ?>
	<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
	<!--[if lt IE 7]>
	<script defer type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/png.js"></script>
	<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie.css";</style>
	<![endif]-->
</head>
<body>
<div id="sword"><img src="<?php print base_path() . path_to_theme() ?>/images/sword.png" alt="" width="84" height="362" class="png" /></div>
<?php if ($logo) { ?><div id="logo"><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a></div><?php } ?>

<div id="headerStuff">
  <div id="links">
	<?php if (isset($secondary_links)) { ?><div id="secondary"><?php print theme('links', $secondary_links) ?></div><?php } ?>
	<?php if (isset($primary_links)) { ?><div id="primary"><?php print theme('links', $primary_links) ?></div><?php } ?>
  </div>
  <div id="searchBar">
	<?php print $search_box ?>
  </div>
</div>

<div><?php print $header ?></div>

<div id="globalBG">
  <div id="tb">
    <div id="rb">
      <div id="bb">
        <div id="lb">
          <div id="t">
            <div id="r">
              <div id="b">
                <div id="l">
                  <div id="text">
                    <div class="columns">
					<div class="a">
						<?php if ($sidebar_left) { ?><div id="sidebar-left">
						  <?php print $sidebar_left ?>
						</div><?php } ?>
						<?php if ($sidebar_right) { ?><div id="sidebar-right">
						  <?php print $sidebar_right ?>
						</div><?php } ?>						
					</div>
					
					<div class="b">
						<?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
						<?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>					
						<?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
						<div id="main">
						<?php print $breadcrumb ?>
						<h1 class="title"><?php print $title ?></h1>
						<div class="tabs"><?php print $tabs ?></div>
						<?php print $help ?>
						<?php print $messages ?>
						<?php print $content; ?>
						</div>					
					</div>
					<span class="clear"></span>
                    </div>
                  </div>
				  <div id="footer">
				    <?php if($footer_message){ print $footer_message . "<br />"; } ?>
				  	Design: <a href="http://www.grossdesign.it/">Gross Design Studio</a><br />
					Powered by: <a href="http://drupal.org/">Drupal</a>
				  </div>
                </div>				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="flag"><img src="<?php print base_path() . path_to_theme() ?>/images/helmsword.png" alt="" width="334" height="204" class="png" /></div>
<!-- Triumviratum Theme Design by Gross Design Studio -->
<?php print $closure ?>
</body>
</html>